﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using MoneyTransfer.Controllers;
using MoneyTransfer.Models;
using MoneyTransfer.Services;
using Moq;
using Xunit;

namespace MoneyTransfer.Tests
{
    public class AccountControllerTests
    {
        [Fact]
        public void GenerationRandNumber()
        {
            IAccountService aController = new AccountService();
            string randNumber = aController.GenerateDigit();
            Assert.Equal( 6, randNumber.Length);
        }
        [Fact]
        public void IsAccountUniq ()
        {
            var mock = new Mock<IAccountService>();
            mock.Setup(repo => repo.GetUsersName()).Returns(GetTestUsersAccount);

            IAccountService aController = new AccountService();
            string randNumber = aController.GenerateDigit();
           // List<string> accounts = aController.GetUsersName();

            Assert.DoesNotContain(randNumber, mock.Name);
        }
        private List<string> GetTestUsersAccount()
        {
            IAccountService aController = new AccountService();
            var users = new List<string>
            {
                aController.GenerateDigit(),
                aController.GenerateDigit(),
                aController.GenerateDigit(),
                aController.GenerateDigit(),
                aController.GenerateDigit(),
                aController.GenerateDigit(),
                aController.GenerateDigit(),

            };
            return users;
        }

    }
}
