﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MoneyTransfer.Models;
using MoneyTransfer.Services;
using Moq;
using Xunit;

namespace MoneyTransfer.Tests
{
    public class PersonAccountControllerTests
    {
        [Fact]
        public void IsExistAccount()
        {
            //var mock = new Mock<IPersonalAccountService>();
          //  mock.Setup(repo => repo.GetUsersName()).Returns(GetTestUsersAccount);
            IPersonalAccountService aController = new PersonalAccountService();
            Task<User> user = aController.IsAccountExistTest("229658");
            
            Assert.NotNull(user);
        }

        [Fact]
        public void IsEnoughMoneyTest()
        {
            
            IPersonalAccountService aController = new PersonalAccountService();
            Task<bool> answer = aController.IsEnoughMoneyTest(5000,"229658");

            Assert.False(answer.Result);
        }

    }
}
