﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using MoneyTransfer.Models;
using MoneyTransfer.Services;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class PersonAccountController : Controller
    {
        private readonly IPersonalAccountService _personalAccountService;
        private readonly UserManager<User> userManager;
        private readonly ApplicationContext context;
        private readonly IStringLocalizer<HomeController> _localizer;

      
        public PersonAccountController(IPersonalAccountService personalAccountService, UserManager<User> userManager, ApplicationContext context, IStringLocalizer<HomeController> localizer)
        {
            _personalAccountService = personalAccountService;
            this.userManager = userManager;
            this.context = context;
            _localizer = localizer;
        }

        [HttpGet]
        public ActionResult Transaction()
        {

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Transaction(TransactionViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (await _personalAccountService.IsAccountExist(model.RecipientAccount))
                {
                    User sender = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
                    if (await _personalAccountService.IsEnoughMoney(model.Balance, sender.UserName))
                    {
                        User reciever = await userManager.FindByNameAsync(model.RecipientAccount);
                        Transaction transaction = new Transaction
                        {
                            SenderId = sender.Id,
                            RecieverId = reciever.Id,
                            Balance = model.Balance,
                            DateOfTransaction = DateTime.Now
                        };
                        sender.Balance -= model.Balance;
                        reciever.Balance += model.Balance;
                        await context.Transactions.AddAsync(transaction);
                        await userManager.UpdateAsync(sender);
                        await userManager.UpdateAsync(reciever);
                        await context.SaveChangesAsync();
                        return RedirectToAction("Transactions", "PersonAccount");
                    }
                    else
                    {
                        return Content(_localizer["NotEnough"]);
                    }
                }
                else
                {
                    return Content(_localizer["NotExist"]);
                }
            }
            return View(model);
        }

        public async Task<IActionResult> Transactions()
        {
           
            User user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            List<Transaction> transactions = await _personalAccountService.Transactions(user);
          
            return View(transactions);
        }

        



    }
}