﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MoneyTransfer.Models;

namespace MoneyTransfer.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly UserManager<User> userManager;
        private ApplicationContext context;

        public HomeController(UserManager<User> userManager, ApplicationContext context)
        {
            this.userManager = userManager;
            this.context = context;
        }

        public async Task<IActionResult> Refill(string userName, int refillQuantity)
        {
            User user = await userManager.FindByNameAsync(userName);
            user.Balance = user.Balance + refillQuantity;
            await userManager.UpdateAsync(user);
            context.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Index()
        {
            User user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return View(user);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
