﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class TransactionViewModel
    {
        

        [Required]
        [Display(Name = "Получитель")]
        public string RecipientAccount { get; set; }

        [Required]
        [Display(Name = "Сумма")]
        public int Balance { get; set; }
    }
}
