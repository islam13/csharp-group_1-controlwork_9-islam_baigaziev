﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace MoneyTransfer.Models
{
    public class Transaction
    {
        [Key]
        public int Id { get; set; }

        public string SenderId { get; set; }
        public User Sender { get; set; }

        public string RecieverId { get; set; }
        public User Reciever { get; set; }

        public int Balance { get; set; }

        public DateTime DateOfTransaction{ get; set; }


    }
}
