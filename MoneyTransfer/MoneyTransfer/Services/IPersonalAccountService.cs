﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;

namespace MoneyTransfer.Services
{
    public interface IPersonalAccountService
    {
      
        Task<bool> IsAccountExist(string recieverAccount);
        Task<bool> IsEnoughMoney(int amount, string senderAccount);
        Task<User> IsAccountExistTest(string recieverAccount);
        Task<List<Transaction>> Transactions(User user);
        Task<bool> IsEnoughMoneyTest(int amount, string senderAccount);
    }

    public class PersonalAccountService : IPersonalAccountService
    {
        private readonly UserManager<User> userManager;
        private readonly ApplicationContext context;

        public PersonalAccountService(UserManager<User> userManager, ApplicationContext context)
        {
            this.userManager = userManager;
            this.context = context;
        }

        public PersonalAccountService()
        {
        }

        public async Task<bool> IsAccountExist(string recieverAccount)
        {
            User user = await userManager.FindByNameAsync(recieverAccount);
            return user != null;
        }
        public async Task<User> IsAccountExistTest(string recieverAccount)
        {
            User user = await userManager.FindByNameAsync(recieverAccount);
            return user;
        }
        public async Task<bool> IsEnoughMoney(int amount, string senderAccount)
        {
            User user = await userManager.FindByNameAsync(senderAccount);
            return user.Balance > amount;
        }
        public async Task<bool> IsEnoughMoneyTest(int amount, string senderAccount)
        {
            User user = await userManager.FindByNameAsync(senderAccount);
            bool answer = user.Balance > amount;
            return answer;
        }


        public async Task<List<Transaction>> Transactions(User user)
        {
            List<Transaction> transactions = 
                await context.Transactions.Where(t => t.RecieverId == user.Id || t.SenderId == user.Id).Include(t=>t.Sender).Include(t=>t.Reciever).ToListAsync();
            return transactions;
        }

       

    }
}
