﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using MoneyTransfer.Models;

namespace MoneyTransfer.Services
{
    public interface IAccountService
    {
        string GenerateUniqueAccount();
        string GenerateDigit(/*Random rnd*/);
        bool IsUnique(string account);
        List<string> GetUsersName();
    }

    public class AccountService : IAccountService
    {
        private readonly UserManager<User> userManager;
        private ApplicationContext context;
        Random rnd = new Random();
        public AccountService(UserManager<User> userManager)
        {
            this.userManager = userManager;
        }

        public AccountService()
        {
            
        }


        public string GenerateDigit(/*Random rnd*/)
        {
           
            string number ="";
            string rand =  rnd.Next(1000, 9999 ).ToString() + DateTime.Now.Second;
            for (int i = 0; i < 6; i++)
            {
                number += rand[i];
            }
            return number;
        }

        public bool IsUnique(string account)
        {
            List<string> usersAccounts = userManager.Users.Select(u => u.UserName).ToList();
            bool isUnique = true;
            foreach (var userAccount in usersAccounts)
            {
                if (userAccount == account)
                {   
                    isUnique = false;
                }
            }
            return isUnique;
        }

        public string GenerateUniqueAccount()
        {

            string number = "";
            bool isUnique = false;
            while (!isUnique)
            {
                number = GenerateDigit(/*rnd*/);
                isUnique = IsUnique(number);
            }
            return number;
        }

        public List<string> GetUsersName()
        {
            List<string> usersAccounts = userManager.Users.Select(u => u.UserName).ToList();
            return usersAccounts;
        }
    }
}